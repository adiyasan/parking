-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 04, 2015 at 12:07 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `parking`
--

-- --------------------------------------------------------

--
-- Table structure for table `entries`
--

CREATE TABLE IF NOT EXISTS `entries` (
`id` int(11) NOT NULL,
  `plate` varchar(30) NOT NULL,
  `model` varchar(50) NOT NULL,
  `note` text NOT NULL,
  `status` varchar(10) NOT NULL DEFAULT 'new',
  `exit_at` timestamp NULL DEFAULT NULL,
  `amount_due` float DEFAULT NULL,
  `change_money` float DEFAULT NULL,
  `paid` float DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `entries`
--

INSERT INTO `entries` (`id`, `plate`, `model`, `note`, `status`, `exit_at`, `amount_due`, `change_money`, `paid`, `created_at`, `updated_at`, `deleted_at`) VALUES
(18, 'adiyasa1', 'mitsubishi pajero sport', '', 'exit', '2015-07-03 21:54:19', 5, 5, 10, '2015-07-03 21:51:39', '2015-07-03 21:54:19', NULL),
(19, 'adiyasa2', 'honda cr-v', '', 'new', NULL, NULL, NULL, NULL, '2015-07-03 21:52:02', '2015-07-03 21:52:02', NULL),
(20, 'adiyasa3', 'toyota alphard', '', 'new', NULL, NULL, NULL, NULL, '2015-07-03 21:52:23', '2015-07-03 21:52:23', NULL),
(21, 'adiyasa4', 'ford escape', '', 'lost', '2015-07-03 22:05:05', 50, 10, 60, '2015-07-03 21:52:39', '2015-07-03 22:05:05', NULL),
(22, 'adiyasa5', 'audi', '', 'new', NULL, NULL, NULL, NULL, '2015-07-03 22:04:39', '2015-07-03 22:04:39', NULL),
(23, 'adiyasa6', 'suzuki karimun wagon', '', 'new', NULL, NULL, NULL, NULL, '2015-07-03 22:05:53', '2015-07-03 22:05:53', NULL),
(24, 'adiyasa7', 'ferrari', '', 'new', NULL, NULL, NULL, NULL, '2015-07-03 22:06:16', '2015-07-03 22:06:16', NULL),
(25, 'adiyasa8', 'lamborghini', '', 'new', NULL, NULL, NULL, NULL, '2015-07-03 22:06:25', '2015-07-03 22:06:25', NULL),
(26, 'adiyasa9', 'lykan', '', 'new', NULL, NULL, NULL, NULL, '2015-07-03 22:06:43', '2015-07-03 22:06:43', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE IF NOT EXISTS `logs` (
`id` int(11) NOT NULL,
  `type` varchar(10) NOT NULL DEFAULT 'entry',
  `entry_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `logs`
--

INSERT INTO `logs` (`id`, `type`, `entry_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'entry', 18, '2015-07-04 04:51:39', NULL, NULL),
(2, 'entry', 19, '2015-07-04 04:52:02', NULL, NULL),
(3, 'entry', 20, '2015-07-04 04:52:23', NULL, NULL),
(4, 'entry', 21, '2015-07-04 04:52:39', NULL, NULL),
(5, 'exit', 18, '2015-07-04 04:54:19', NULL, NULL),
(6, 'entry', 22, '2015-07-04 05:04:39', NULL, NULL),
(7, 'lost', 21, '2015-07-04 05:05:05', NULL, NULL),
(8, 'entry', 23, '2015-07-04 05:05:54', NULL, NULL),
(9, 'entry', 24, '2015-07-04 05:06:16', NULL, NULL),
(10, 'entry', 25, '2015-07-04 05:06:25', NULL, NULL),
(11, 'entry', 26, '2015-07-04 05:06:43', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `entries`
--
ALTER TABLE `entries`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
 ADD PRIMARY KEY (`id`), ADD KEY `entry_id` (`entry_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `entries`
--
ALTER TABLE `entries`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `logs`
--
ALTER TABLE `logs`
ADD CONSTRAINT `logs_ibfk_1` FOREIGN KEY (`entry_id`) REFERENCES `entries` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
