This is the instructions to run parking system.
1. Create database 'parking' in mysql
2. Restore file 'parking.sql'
3. Put 'parking' folder in webroot
4. Run webserver, apache / nginx
5. Open terminal / command line in this folder and then run 'php artisan serve'
6. Open mozilla / chrome then type 'localhost:8000' in address bar.