<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="token" content="{{csrf_token()}}">        
        <title>Parking System</title>        
        <LINK rel="shortcut icon" href="favicon.ico" title="Favicon"/>
        <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" media="screen" />
        <link href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" media="screen" />
        <link href="{{asset('css/style.css')}}" rel="stylesheet" type="text/css" media="screen" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        
        @if(Session::has('flash-danger'))
            <div class="alert alert-danger alert-dismissable" style="cursor:pointer;" onclick="$(this).fadeOut();return false;">
                <i class="fa fa-ban"></i>
                <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                {!!Session::get('flash-danger')!!}
            </div>
        @endif

        <section>
            <div class="container main">
                <div class="col-md-4 menu-container">
                    <div class="container-fluid time-container">
                      <div class="pull-left">
                        <strong>Date {{ date('Y-m-d') }}</strong>
                      </div>
                      <div class="pull-right">
                        <strong>Time <span id="time"></span></strong>
                      </div>
                    </div>
                    <div class="panel panel-primary">
                      <div class="panel-heading">
                        <h3 class="panel-title">Entry</h3>
                      </div>
                      <div class="panel-body">
                        <div class="container-fluid">
                            <form class="form-horizontal" role="form" method="post" id="form-entry" name="form-entry">
                                <div class="form-group">
                                    <label class="col-md-4 control-label"><strong>Plate</strong></label>
                                    <div class="col-md-8">
                                      <input type="text" class="form-control required" id="plate" name="plate" autofocus />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label"><strong>Model</strong></label>
                                    <div class="col-md-8">
                                      <input type="text" class="form-control required" id="model" name="model">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label"><strong>Note</strong><span class="help-block">optional</span></label>
                                    <div class="col-md-8">
                                      <textarea class="form-control" id="note" name="note"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-8 col-md-offset-4">
                                      <button type="button" class="btn btn-primary" id="submit-entry"><i class="fa fa-plus"></i> Add</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                      </div>
                    </div>
                    <div class="panel panel-primary">
                      <div class="panel-heading">
                        <h3 class="panel-title">Exit</h3>
                      </div>
                      <div class="panel-body">
                        <form class="form-horizontal" role="form" method="post" id="form-exit" name="form-exit">
                          <div class="form-group">
                              <label class="col-md-4 control-label"><strong>No. Ticket</strong></label>
                              <div class="col-md-8">
                                <input type="text" class="form-control required" id="no_ticket" name="no_ticket">
                              </div>
                          </div>
                          <div class="form-group">
                              <div class="col-md-8 col-md-offset-4">
                                <button type="button" class="btn btn-primary" id="submit-exit"><i class="fa fa-arrow-circle-o-right"></i> Exit</button>
                              </div>
                          </div>
                        </form>
                      </div>
                    </div>
                    <div class="panel panel-primary">
                      <div class="panel-heading">
                        <h3 class="panel-title">Lost Ticket</h3>
                      </div>
                      <div class="panel-body">
                        <form class="form-horizontal" role="form" method="get" id="form-search" name="form-search">
                          <div class="form-group">
                              <label class="col-md-4 control-label"><strong>Plate</strong></label>
                              <div class="col-md-8">
                                <input type="text" class="form-control" id="plate_search" name="plate_search" placeholder="Type all or part then enter">
                              </div>
                          </div>
                          <div class="form-group">
                              <div class="col-md-8 col-md-offset-4">
                                <button type="button" class="btn btn-primary" id="submit-search"><i class="fa fa-search"></i> Search</button>
                              </div>
                          </div>
                        </form>
                      </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <h2 class="text-center">Daily Log</h2>
                    <div class="table-container">
                    </div>
                </div>
            </div>            
        </section>
        
        <!-- modals -->
        <div class="modal fade" id="modal-pay" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" aria-labelledby="modal-pay-label" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="modal-pay-label">Exit</h4>
                    </div>
                    <div class="modal-body">
                        <div id='div-alert-msg'></div>
                        <div class="container-fluid">
                            <form class="form-horizontal" role="form" id="form-pay" name="form-pay" method="post">
                                <input type="hidden" id="id" name="id" />
                                <input type="hidden" id="type" name="type" />
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Plate</label>
                                    <div class="col-md-8">
                                        <p class="form-control-static" id="plate"></p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Model</label>
                                    <div class="col-md-8">
                                        <p class="form-control-static" id="model"></p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Note</label>
                                    <div class="col-md-8">
                                        <p class="form-control-static" id="note"></p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">EntryTime</label>
                                    <div class="col-md-8">
                                        <p class="form-control-static" id="entry-time"></p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Time Spent</label>
                                    <div class="col-md-8">
                                        <p class="form-control-static" id="spent-time"></p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Amount Due</label>
                                    <div class="col-md-8">
                                        <div class="input-group">
                                            <div class="input-group-addon">$</div>
                                            <input type="text" readonly id="amount-due" class="form-control" value="0" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Paid</label>
                                    <div class="col-md-8">
                                        <div class="input-group">
                                            <div class="input-group-addon">$</div>
                                            <input type="text" id="paid" name="paid" class="form-control required" value="0" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Change</label>
                                    <div class="col-md-8">
                                        <div class="input-group">
                                            <div class="input-group-addon">$</div>
                                            <input readonly type="text" class="form-control" id="change" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-8 col-md-offset-4">
                                        <button class="btn btn-primary" type="button" id="submit-pay" data-loading-text="Please wait...">
                                            <i class="fa fa-send-o"></i> PAY
                                        </button>
                                        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Cancel</button>
                                    </label>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal-search" tabindex="-1" role="dialog" aria-labelledby="modal-search-label" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h3 class="modal-title">Search Vehicle</h3>
              </div>
              <div class="modal-body">
                
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>
        <!--end of modals-->

        <!--busy indicator for ajax requests-->
        <div id='busy-indicator'>
            <img src="{{asset('images/ajax-loader.gif')}}" />
        </div>
        <!--end of busy indicator-->

        <script src="{{asset('js/jquery-2.1.4.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('js/bootstrap.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('js/jquery.validate.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('js/default.js')}}" type="text/javascript"></script>
        <script type="text/javascript">
            $(window).load(function(e) {
                refreshTable();
                startTime();

                //set paid = 0 when modal hidden
                $('#modal-pay').on('hidden.bs.modal', function (e) {
                    $('#modal-pay #form-pay #paid').val(0);
                });

                //set focus to paid field when modal shown
                $('#modal-pay').on('shown.bs.modal', function () {
                    $('#modal-pay #form-pay #paid').focus();
                });
            });

            $(document).ready(function(e) {
                /* table */
                //ajax pagination for daily log table
                $(".table-container").on('click','.pagination li a',function(evt) {
                  var obj = $(this); 
                  refreshTable(obj.attr('href'));
                  evt.preventDefault();
                });

                //ajax pagination for search result table
                $("#modal-search .modal-body").on('click','.pagination li a',function(evt) {
                  var obj = $(this); 
                  loadSearchResult(obj.attr('href'));
                  evt.preventDefault();
                }); 
                /* end table */

                /* entry */
                //validate form entry
                $("#form-entry").validate();

                //submit entry when user press enter on input field
                $("#form-entry").on('keypress','input',function(evt) {
                  if(evt.keyCode==13) {
                    $('#submit-entry').trigger('click');
                    evt.preventDefault();
                  }
                });

                //action for submit entry
                $("#submit-entry").on('click',function(evt) {
                  var obj = $(this);
                  if($("#form-entry").valid()) {
                    $.ajax({
                      dataType:"json",
                      type:"post",
                      data:$('#form-entry').serialize(),
                      beforeSend:function (XMLHttpRequest) {
                        $('#busy-indicator').show();
                        obj.button('loading');
                        return XMLHttpRequest.setRequestHeader('X-CSRF-TOKEN', $("meta[name='token']").attr('content'));
                      },
                      complete:function (XMLHttpRequest, textStatus) {
                        obj.button('reset');
                        $('#busy-indicator').hide();
                      },
                      success:function (data, textStatus) {
                        if(data.status=='success') {
                          $('#form-entry').clearForm();
                          refreshTable();
                        }
                        else if(data.status=='duplicate') {
                          $('#form-entry').clearForm();
                          alert('Duplicate input for plate! Plate has already recorded in database.');
                        }
                      },
                      url : "{{route('entry')}}"
                    });
                  }
                });
                /* end entry */

                /* exit */
                //validate form exit
                $("#form-exit").validate();

                //submit entry when user press enter on input field
                $("#form-exit").on('keypress','input',function(evt) {
                  if(evt.keyCode==13) {
                    $('#submit-exit').trigger('click');
                    evt.preventDefault();
                  }                  
                });

                //action for submit exit
                $("#submit-exit").on('click',function(evt) {
                  var obj = $(this);
                  if($("#form-exit").valid()) {
                    $.ajax({
                      dataType:"json",
                      type:"post",
                      data:$('#form-exit').serialize(),
                      beforeSend:function (XMLHttpRequest) {
                        $('#busy-indicator').show();
                        obj.button('loading');
                        return XMLHttpRequest.setRequestHeader('X-CSRF-TOKEN', $("meta[name='token']").attr('content'));
                      },
                      complete:function (XMLHttpRequest, textStatus) {
                        obj.button('reset');
                        $('#busy-indicator').hide();
                      },
                      success:function (data, textStatus) {
                        if(data.status=='success') {
                          openPayDialog(data.data, 'exit');
                        }
                        else if(data.status=='failed') {
                          alert('Ticket expired! Ticket has been used.');
                        }
                        else if(data.status=='wrong') {
                          alert('Wrong ticket number!');
                        }
                      },
                      url : "{{route('exit')}}"
                    });
                  }
                });
                
                //calculate the change when user type in paid field
                $('#modal-pay').on('keyup','#form-pay #paid',function(evt) {
                    var obj = $(this);
                    var change = parseFloat(obj.val())-$('#modal-pay #form-pay #amount-due').val();
                    $('#modal-pay #form-pay #change').val(change);
                });

                //submit pay when user press enter on paid field
                $('#modal-pay').on('keypress','input[type=text]',function(evt) {
                    if(evt.keyCode==13) {
                        $('#modal-pay #form-pay #submit-pay').trigger('click');    
                    }
                });

                //action for submit pay
                $("#submit-pay").on('click',function(evt) {
                    var obj = $(this);
                    if($("#form-pay").valid()) {
                        $.ajax({
                          type:"post",
                          dataType:"json",
                          data:$('#form-pay').serialize(),
                          beforeSend:function (XMLHttpRequest) {
                            $('#busy-indicator').show();
                            obj.button('loading');
                            return XMLHttpRequest.setRequestHeader('X-CSRF-TOKEN', $("meta[name='token']").attr('content'));
                          },
                          complete:function (XMLHttpRequest, textStatus) {
                            obj.button('reset');
                            $('#busy-indicator').hide();
                          },
                          success:function (data, textStatus) {
                            if(data.status=='success') {
                              $('#modal-pay').modal('hide');
                              refreshTable();
                            }
                            else if(data.status=='failed') {
                              alert('Ticket expired! Ticket has been used.');
                            }
                            else if(data.status=='wrong') {
                              alert('Wrong ticket number!');
                            }
                          },
                          url : "{{route('postPay')}}"
                        });
                    }
                });
                /* end exit */

                /* search */
                //search plate when user press enter on search field
                $("#form-search").on('keypress','input',function(evt) {
                  if(evt.keyCode==13) {
                    $('#submit-search').trigger('click');
                    evt.preventDefault();
                  }
                });

                //action for submit search
                $("#submit-search").on('click',function(evt) {
                    var obj = $(this);
                    $.ajax({
                      dataType:"json",
                      data:$('#form-search').serialize(),
                      beforeSend:function (XMLHttpRequest) {
                        $('#busy-indicator').show();
                        obj.button('loading');
                      },
                      complete:function (XMLHttpRequest, textStatus) {
                        obj.button('reset');
                        $('#busy-indicator').hide();
                      },
                      success:function (data, textStatus) {
                        //if all of plate keyword is match with any record then open pay dialog
                        if(data.matched==1) {
                          openPayDialog(data.data, 'lost');
                        }
                        //else open search results
                        else {
                            $('#modal-search #keyword').html($('#plate_search').val());
                            $('#modal-search').modal('show');
                            loadSearchResult();
                        }
                      },
                      url : "{{route('search')}}"
                    });
                });
                
                //action when user pick a vehicle on search result's table
                $("#modal-search").on('click','.pick',function(evt) {                    
                    var obj = $(this);                    
                    $("#modal-search").modal('hide');
                    $.ajax({
                      dataType:"json",
                      type:"post",
                      data: {
                        no_ticket : obj.attr('data-id'),
                        type      : 'lost'
                      },
                      beforeSend:function (XMLHttpRequest) {
                        $('#busy-indicator').show();
                        return XMLHttpRequest.setRequestHeader('X-CSRF-TOKEN', $("meta[name='token']").attr('content'));
                      },
                      complete:function (XMLHttpRequest, textStatus) {
                        $('#busy-indicator').hide();
                      },
                      success:function (data, textStatus) {
                        if(data.status=='success') {
                          openPayDialog(data.data, 'lost');
                        }
                        else {
                          alert('Wrong data!');
                        }
                      },
                      url : "{{route('exit')}}"
                    });
                });
                /* end search */
            });
            
            //function for opening pay modal
            function openPayDialog(data, type) {
              $('#modal-pay #form-pay #id').val(data.id);
              $('#modal-pay #form-pay #type').val(type);
              $('#modal-pay #form-pay #plate').html(data.plate);
              $('#modal-pay #form-pay #model').html(data.model);
              $('#modal-pay #form-pay #note').html(data.note);
              $('#modal-pay #form-pay #entry-time').html(data.entry_date);
              $('#modal-pay #form-pay #spent-time').html(data.spent_time);
              $('#modal-pay #form-pay #amount-due').val(data.amount_due);
              $('#modal-pay #modal-pay-label').html(type.toUpperCase());
              //calc change
              var change = parseFloat($('#modal-pay #form-pay #paid').val())-data.amount_due;
              $('#modal-pay #form-pay #change').val(change);
              $('#modal-pay').modal('show');
            }

            //function for load search results
            function loadSearchResult(url) {
              var urlForPagination = url==undefined?"{{route('tableResult')}}":url;
                $.ajax({
                  data: {
                    q : $('#plate_search').val()
                  },
                  dataType:"html",
                  beforeSend:function (XMLHttpRequest) {
                    $('#busy-indicator').show();
                  },
                  complete:function (XMLHttpRequest, textStatus) {
                    $('#busy-indicator').hide();
                  },
                  success:function (data, textStatus) {
                    $('#modal-search .modal-body').html(data);                    
                  },
                  url : urlForPagination
                });
            }

            //function for refresh daily log's table
            function refreshTable(url) {
              var urlForPagination = url==undefined?"{{route('refresh')}}":url;
                $.ajax({
                  dataType:"html",
                  beforeSend:function (XMLHttpRequest) {
                    $('#busy-indicator').show();
                  },
                  complete:function (XMLHttpRequest, textStatus) {
                    $('#busy-indicator').hide();
                  },
                  success:function (data, textStatus) {
                    $('.table-container').html(data);                    
                  },
                  url : urlForPagination
                });
            }
              
            //function for displaying digital clock
            function startTime() {
                var today = new Date();
                var h = today.getHours();
                var m = today.getMinutes();
                var s = today.getSeconds();

                // add a zero in front of numbers<10
                m = checkTime(m);
                s = checkTime(s);

                //Check for PM and AM
                var day_or_night = (h > 11) ? "PM" : "AM";

                //Convert to 12 hours system
                if (h > 12)
                    h -= 12;

                //Add time to the headline and update every 500 milliseconds
                $('#time').html(h + ":" + m + ":" + s + " " + day_or_night);
                setTimeout(function() {
                    startTime()
                }, 500);
            }

            //function for adding zero on minutes /seconds
            function checkTime(i) {
                if (i < 10)
                {
                    i = "0" + i;
                }
                return i;
            }
        </script>
    </body>
</html>