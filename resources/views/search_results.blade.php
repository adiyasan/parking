<h4>Found <strong>{{ $data->total() }}</strong> vehicle(s) with plate contains : <strong>{{ $q }}</strong></h4>

@if($data->total()>0)
  <div class="table-responsive">
    <table class="table table-hover table-striped">
      <thead>
        <tr>
          <th><span class="text-center"><strong>No.</strong></th>
          <th><span class="text-center"><strong>Plate</strong></th>        
          <th><span class="text-center"><strong>Model</strong></th>
          <th><span class="text-center"><strong>Note</strong></th>
          <th><span class="text-center"><strong>Entry Time</strong></th>
          <th><span class="text-center"><strong>No. Ticket</strong></th>
          <th><span class="text-center"><strong>Action</strong></th>
        </tr>
      </thead>
      <tbody>
      @foreach($data as $i=>$item)
        <?php $number = $data->currentPage()*$data->perPage()-$data->perPage()+$i+1; ?>
        <tr>
          <td>{{ $number }}</td>
          <td>{!! highlight($item->plate, $q, STR_HIGHLIGHT_SIMPLE, '<mark>\1</mark>') !!}</td>
          <td>{{ $item->model }}</td>
          <td>{{ $item->note }}</td>
          <td>{{ $item->created_at->format('Y-m-d g:i:s A') }}</td>
          <td>{{ $item->id }}</td>
          <td><a href="#" class="btn btn-info pick" data-id="{{ $item->id }}" title="Pick this vehicle to pay">pick</a></td>
        </tr>
      @endforeach
      </tbody>
    </table>
  </div>

  <div class="text-center">
    {!! $data->appends(['q' => $q])->render() !!}
  </div>
@endif