<div class="table-responsive">
  <table class="table table-hover table-striped">
    <thead>
      <tr>
        <th><span class="text-center"><strong>No.</strong></th>
        <th><span class="text-center"><strong>Type</strong></th>
        <th><span class="text-center"><strong>No. Ticket</strong></th>
        <th><span class="text-center"><strong>Time</strong></th>
        <th><span class="text-center"><strong>Plate</strong></th>
        <th><span class="text-center"><strong>Model</strong></th>
        <th><span class="text-center"><strong>Note</strong></th>
      </tr>
    </thead>
    <tbody>
    @foreach($data as $i=>$item)
      <?php 
        $number = $data->currentPage()*$data->perPage()-$data->perPage()+$i+1;       
        if($item->type=='exit') {
          $label = '<span class="label label-success">Exit</span>';
        }
        else if($item->type=='lost') {
          $label = '<span class="label label-danger">Lost</span>';
        }
        else {
          $label = '<span class="label label-primary">Entry</span>';
        }
      ?>
      <tr>
        <td>{{ $number }}</td>
        <td>{!! $label !!}</td>
        <td>{{ $item->id }}</td>
        <td>{{ Carbon\Carbon::createFromTimestamp(strtotime($item->log_date))->format('Y-m-d g:i:s A') }}</td>
        <td>{{ $item->plate }}</td>
        <td>{{ $item->model }}</td>
        <td>{{ $item->note }}</td>
      </tr>
    @endforeach
    </tbody>
  </table>
</div>

<div class="text-center">
  {!! $data->render() !!}
</div>