<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Entry extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'entries';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['plate', 'model', 'note', 'status', 'exit_at', 'amount_due', 'paid', 'change_money'];

    public function log() {
        return $this->hasMany('App\Log','entry_id');
    }
        
}
