<?php 
namespace App\Http\Controllers;

use Carbon\Carbon;
use Request;
use App\Log;
use App\Entry;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller 
{

	/*
	|--------------------------------------------------------------------------
	| Default Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{

	}

	/**
	 * Show index.
	 *
	 * @return view
	 */
	public function getIndex() {
		return view('index');
	}

	/**
	 * This is for adding entry.
	 *
	 * @return json status
	 */
	public function postEntry() {
		if(Request::ajax()) {
			//check possible duplicate of plate
			$check = Entry::where('plate',Request::input('plate'))->where('status','new')->first();
			if($check == null) {
				//add entry
				$entry 			= new Entry;
				$entry->plate 	= Request::input('plate');
				$entry->model 	= Request::input('model');
				$entry->note  	= Request::input('note');
				$entry->save();
				//add to log
				$entry->log()->insert(['type' => 'entry', 'entry_id' => $entry->id]);

				return response()->json(['status' => 'success']);
			}
			else {
				return response()->json(['status' => 'duplicate']);
			}
		}
	}

	/**
	 * This is for adding exit / lost.
	 *
	 * @return json status
	 */
	public function postExit() {
		if(Request::ajax()) {
			$entry 		= Entry::find(Request::input('no_ticket'));
			if($entry != null) {
				if($entry->status != 'new') {
					return response()->json(['status' => 'failed']);
				}
				else {
					$created 			= new Carbon($entry->created_at);
					$now 				= Carbon::now();
					$diffInMinutes 		= $created->diffInMinutes($now);
                    $diffHours 			= floor($diffInMinutes/60);
                    $diffMinutes 		= $diffInMinutes%60;
                    
                    //spent time is in diff hours and minutes
                    $spent_time 		= "$diffHours hour(s) $diffMinutes minute(s)";
                                        
                    //if lost
                    if(Request::has('type') && Request::input('type')=='lost') {
                    	//the charge for lost is flat = $50
                    	$amount_due 	= 50;
                    }
                    //else, normal exit
                    else {
                    	//the rate a hour is $5, spent time is round up
                    	$amount_due 	= ceil($diffInMinutes/60)*5;
                    }

                    //entry date in AM/PM format
                    $entry_date 		= $entry->created_at->format('Y-m-d g:i:s A');

                    $data 				= $entry->toArray();
                    $data['entry_date'] = $entry_date;
                    $data['spent_time'] = $spent_time;
                    $data['amount_due'] = $amount_due;

                    return response()->json(['status'		=> 'success',
											'data'			=> $data]);
				}
			}
			return response()->json(['status' => 'wrong']);
		}
	}

	/**
	 * This is for searching plate when lost ticket.
	 *
	 * @return json status: matched / notmatched
	 */
	public function getSearch() {
		$keyword = Request::input('plate_search');
		//check for all chars matched plate
		$entry = Entry::whereRaw(DB::raw(" lower(plate)='".safely(strtolower($keyword))."' "))
						->where('status', 'new')
						->first();
		if($entry != null) {
			$created 			= new Carbon($entry->created_at);
			$now 				= Carbon::now();
			$diffInMinutes 		= $created->diffInMinutes($now);
            $diffHours 			= floor($diffInMinutes/60);
            $diffMinutes 		= $diffInMinutes%60;
            
            //spent time is in diff hours and minutes
            $spent_time 		= "$diffHours hour(s) $diffMinutes minute(s)";
            
            //the charge for lost is flat = $50
            $amount_due 		= 50;

            //entry date in AM/PM format
            $entry_date 		= $entry->created_at->format('Y-m-d g:i:s A');

			$data 				= $entry->toArray();
            $data['entry_date'] = $entry_date;
            $data['spent_time'] = $spent_time;
            $data['amount_due'] = $amount_due;

			return response()->json(['matched' => 1, 'data' => $data]);
		}
		//if nothing matched then search with like
		else {
			return response()->json(['matched' => 0]);
		}
	}

	/**
	 * This is for generating view of search results.
	 *
	 * @return view
	 */
	public function getTableSearch() {
		if(Request::ajax()) {
			$data = Entry::whereRaw(DB::raw(" lower(plate) LIKE '%".safely(strtolower(Request::input('q')))."%' "))
							->where('status', 'new')
							->orderBy('created_at', 'desc')
							->paginate(PER_PAGE);
			return view('search_results', ['data' => $data, 'q' => Request::input('q')]);
		}
	}

	/**
	 * This is for pay exit/lost.
	 *
	 * @return json status
	 */
	public function postPay() {
		if(Request::ajax()) {
			$entry 		= Entry::find(intval(Request::input('id')));
			if($entry != null) {
				if($entry->status != 'new') {
					return response()->json(['status' => 'failed']);
				}
				else {
					$type = Request::input('type');
					$paid = floatval(Request::input('paid'));

					if($type == 'exit') {
						//recalculate the rate
						$created 			= new Carbon($entry->created_at);
						$now 				= Carbon::now();
						$diffInMinutes 		= $created->diffInMinutes($now);
	                    $diffHours 			= floor($diffInMinutes/60);
	                    $diffMinutes 		= $diffInMinutes%60;
	                    
	                    //the rate a hour is $5, spent time is round up
	                    $amount_due 		= ceil($diffInMinutes/60)*5;
	                }
	                else {
	                	//the charge for lost is flat = $50
	                	$amount_due 		= 50;
	                }
	                //update the entry
	                $entry->exit_at 		= Carbon::now();
	                $entry->amount_due 		= $amount_due;	                    
	                $entry->paid 			= $paid;
	                $entry->change_money 	= ($paid - $amount_due);
	                $entry->status 			= $type;
	                $entry->save();

	                //add to log
	                $entry->log()->insert(['type' => $type, 'entry_id' => $entry->id]);
	                
                    return response()->json(['status' => 'success']);
				}
			}
			return response()->json(['status' => 'wrong']);
		}
	}

	/**
	 * This is for generating daily log table.
	 *
	 * @return view
	 */
	public function getTable() {
		if(Request::ajax()) {
			$data = Log::leftJoin('entries', 'logs.entry_id', '=', 'entries.id')
						->selectRaw(DB::raw('entries.*, logs.type, logs.created_at log_date'))
						->orderBy('log_date', 'desc')->paginate(PER_PAGE);
			return view('table', ['data' => $data]);
		}
	}

}
