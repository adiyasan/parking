<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/',array('as'=>'index','uses'=>'HomeController@getIndex'));
Route::post('/entry',array('as'=>'entry','uses'=>'HomeController@postEntry'));
Route::post('/exit',array('as'=>'exit','uses'=>'HomeController@postExit'));
Route::get('/search_plate',array('as'=>'search','uses'=>'HomeController@getSearch'));
Route::get('/get_pay',array('as'=>'getPay','uses'=>'HomeController@getPay'));
Route::post('/pay',array('as'=>'postPay','uses'=>'HomeController@postPay'));
Route::get('/refresh_table',array('as'=>'refresh','uses'=>'HomeController@getTable'));
Route::get('/search_result',array('as'=>'tableResult','uses'=>'HomeController@getTableSearch'));